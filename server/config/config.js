module.exports = {
    'base_url': 'http://localhost:90/',
    'app_title': 'NEM NETWORK EXPLORER',
    'nis_base_url': 'http://47.107.245.217:3000/',
    'api_network_status': "https://www.nodeexplorer.com/api_network_status",
    'api_address': [
        "13.114.200.132",
        "40.90.163.184",
        "52.194.207.217",
        "47.107.245.217",
        "18.228.92.190"
    ],
    'networktime' : 1459468800000
}